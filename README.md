# Evenement #


L'évènement se réparti sur un nombre X d'étapes. A chaque étape, vous aurez le thème de l'étape et 4 informations de type "zone", "point de départ", "direction", "indice". Le but sera de donner la position de l'indice.

## Indices ##

* La "zone" permet de savoir où se trouve l'indice ;
* Le "point de départ" permet d'avoir une position précise ;
* "direction" permet de savoir la direction (à savoir, que vous pouvez contourner pour continuer dans la direction) ;
* "indice" est présent sur la map (celle ci peut être vague).

## Comptage des points ##
* Les points se font par décrementation. Le premier gagne X points, le second gagne X-2 points, etc.
* 3 minutes après que le premier à trouver, tous ceux qui ne trouvent pas gagnent 0 points.
* Le partage sera fait le lendemain.

### Etape 4: Série Wakfu [20,-28] ###
* Zone: L'eau salé a provoqué l'apparition de zombies.
* Point de départ: Proche d'un zaap. Je suis devenu temporairement roi grâce à un jeu qui s'approche du poker.
* Direction: Par rapport à la zone la direction est celle de l'être vaincu deux fois par Noxi dans la série.
* Indice: Une créature qu'on verra pratiquement que dans quelques épisodes dont le début de la saison 1 et 2.

### Etape 11: Matériaux [-13,-23] ###
* Zone: Les monstres sont fait de frène et de fer.
* Point de départ: Peut utiliser de la poudre explosive de gourlo.
* Direction: Description de la Silicate
* Indice: Les couleurs proches du magnésite.

### Etape 10: Machin truc [-15,2] ###
* Zone: Le zaap central pour beaucoup de personnes.
* Point de départ: Triple quête !
* Direction: Dans la direction opposé à celle impossible.
* Indice: Sa produit de la lumière (probablement)

### Etape 9: Classiques d'animation Disney [-20,21] ###
* Zone: Les 101 dalmatiens en terme d'ambiance, mais plus verdâtre avec des loups à la place.
* Point de départ: Ressemblance fortuite à un personnage pressé d'Alice au pays des merveilles.
* Direction: Dans la direction opposé de la scène du chasseur de Bambi
* Indice: Robin des bois l'a percé.

###  Etape 8: Nombre. Je sais que vous aimez sa. [-34,-89] ###
* Zone: 5 janvier.
* Point de départ: 110.
* Direction: 50.
* Indice: (180+Sin(-180°)*sqrt(42)+Log(1)*log(exp(69)))/cos(360)


### Etape 7: Liquide à consommer avec modération. [11,25] ###
* Zone: L'eau est la solution à l'alcool.
* Point de départ: Liquide biologique comestible.
* Direction: Du jus hautement non comestible.
* Indice: Jaunâtre, probablement du jus de citron.


### Etape 6: Couleurs [-21,35] ###
* Zone: Je suis souvent representé par le rouge et le noir
* Point de départ: Souvent de couleur or.
* Direction: Vert crâde serait-il la couleur ?
* Indice: Violet.

### Etape 5: Nombre. Juste Nombre. Non nécessaire de se déplacer. Juste aller sur votre map [22,-5] ###
* Zone: 4
* Point de départ: 5
* Direction: 2
* Indice: 2

### Etape 3: La minimap. [-7,-39] ###
* Zone: La tour du tour du monde.
* Point de départ: Des palmiers à l'infini, je suis le plus proche de la tour dans la zone.
* Direction: Par rapport au Point de départ, en continuant, tu trouves un grands 8
* Indice: Une intersection parmi tant d'autres.

### Etape 2: Des restrictions en pagaille. [-64,-48] ###
* Zone: Je n'y suis accessible que à partir du niveau 50
* Point de départ: Restreint les personnes incapables de heal un tant soi peu
* Direction: Par rapport au Point de départ, mon usage est restreint par quel alliance me possède.
* Indice: Vous êtes obligé de passer par leur boutique pour jouer sur ce serveur.

### Etape 1: Des équipements en pagaille. [-29,-53] ###
* Zone: Je suis fait de boisaille, 3 PA, je dois être fabriquer pour commencer une longue série de quête.
* Point de départ: Fabriqué à cette endroit, de niveau 110, je remplace très souvent un équipement 45.
* Direction: Même type d'item que le Point de départ, mais je suis un légendaire, mon nom commence par la direction.
* Indice: Epée possèdant le nom de la créature.


### Etape 0 (Test): Absence de thème. [0,-2] ###

* Zone: Tout départ commence ici.
* Point de départ: Si tu as beaucoup bu, plouf tu ferras et une pécheuse rigoleras.
* Direction: Sauf si ton orientation est mauvaise, tu retourneras boire.
* Indice: Ce ne sont surement pas les tiennes, mais surement plus sec que les tiennes.
